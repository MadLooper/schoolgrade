/**
 * Created by KW on 7/08/2017.
 */

public enum SubjectType {
	MATH, PE, ENGLISH, IT
}
