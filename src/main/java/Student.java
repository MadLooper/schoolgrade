/**
 * Created by KW on 7/08/2017.
 */


public class Student {
	private String name, surname, pesel;

	public Student(String name, String surname, String pesel) {
		super();
		this.name = name;
		this.surname = surname;
		this.pesel = pesel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", surname=" + surname + ", pesel=" + pesel + "]";
	}

	
}
