import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
/**
 * Created by KW on 7/08/2017.
 */
public class SchoolSubject {
	private Map<Integer, List<Grade>> grades = new HashMap<Integer, List<Grade>>();
	
	public SchoolSubject(int studentNumber){
		for(int i=0; i<studentNumber; i++){
			grades.put(i, new LinkedList<>());
		}
	}
	
	public void addGrade(int id, Grade g){
		System.out.println("Dodaje ocene " + g.getGrade() + " uczniowi o id: " + id);
		if(grades.containsKey(id)){
			grades.get(id).add(g);
		}else{
			grades.put(id, new ArrayList<Grade>());
			grades.get(id).add(g);
		}
	}
	
	public void printAllOut(){
		for(Entry<Integer, List<Grade>> ocenyUcznia : grades.entrySet()){
			System.out.println("Uczen id :" + ocenyUcznia.getKey() + " => " + ocenyUcznia.getValue());
		}
	}
}
